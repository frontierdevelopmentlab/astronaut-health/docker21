# Frontier Development Lab'21
# ___placeholder for fancy project name
#
# Frank Soboczenski, June 2021

FROM alpine:3.7
#FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04

MAINTAINER "fs <frank.soboczenski@gmail.com>"

ENV ALPINE_VERSION=3.8

ENV PACKAGES="\
  dumb-init \
  bash \
  ca-certificates \
  python3-dev \
"
RUN apk update && apk add --no-cache $PACKAGES && rm -rf /var/cache/apk/*

COPY . /home
WORKDIR /home

#RUN pip3 install -r requirements.txt

#CMD [ "python3", "-u", "hello.py"]

CMD tail -f /dev/null
