#!/usr/bin/env bash
if docker build -t docker_demo .  ; then
  docker tag docker_demo gcr.io/fdl-us-astronaut-health/docker_demo
  docker push gcr.io/fdl-us-astronaut-health/docker_demo

  docker tag frank_demo registry.gitlab.com/frontierdevelopmentlab/astronaut-health/pipeline1
  docker push registry.gitlab.com/frontierdevelopmentlab/astronaut-health/pipeline1
fi
